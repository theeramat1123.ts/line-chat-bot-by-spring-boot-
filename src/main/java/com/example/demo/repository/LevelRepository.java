package com.example.demo.repository;

import com.example.demo.entity.LevelEntity;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface LevelRepository extends CrudRepository<LevelEntity, Long> {
    LevelEntity findByLevel(@Param("level") int level);
}