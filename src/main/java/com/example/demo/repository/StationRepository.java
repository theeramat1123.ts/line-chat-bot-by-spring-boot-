package com.example.demo.repository;

import com.example.demo.entity.*;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import java.util.List;

public interface StationRepository extends CrudRepository<StationEntity, Long> {

}
