package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

import javax.annotation.PostConstruct;

import com.example.demo.entity.LevelEntity;
import com.example.demo.entity.StationEntity;
import com.example.demo.repository.LevelRepository;
import com.example.demo.repository.StationRepository;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.LocationMessageContent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import com.example.demo.service.PmService;

@SpringBootApplication
@LineMessageHandler
public class DemoApplication {

	@Autowired
	private StationRepository stationRepository;

	@Autowired
	private LevelRepository levelRepository;

	@Autowired
	private PmService service;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@PostConstruct
	public void init() {
		stationRepository.save(new StationEntity(1, "ลาดกระบัง", 50 ,13.726307 ,100.776454));
		levelRepository.save(new LevelEntity(1,1,"คุณภาพอากาศดีมาก","คุณภาพอากาศดีมาก เหมาะสำหรับกิจกรรมกลางแจ้งและการท่องเที่ยว"));
		levelRepository.save(new LevelEntity(2,2,"คุณภาพอากาศดีมาก","คุณภาพอากาศดีมาก เหมาะสำหรับกิจกรรมกลางแจ้งและการท่องเที่ยว"));
		levelRepository.save(new LevelEntity(3,3,"คุณภาพอากาศดีมาก","คุณภาพอากาศดีมาก เหมาะสำหรับกิจกรรมกลางแจ้งและการท่องเที่ยว"));
		levelRepository.save(new LevelEntity(4,4,"คุณภาพอากาศดีมาก","คุณภาพอากาศดีมาก เหมาะสำหรับกิจกรรมกลางแจ้งและการท่องเที่ยว"));
		levelRepository.save(new LevelEntity(5,5,"คุณภาพอากาศดีมาก","คุณภาพอากาศดีมาก เหมาะสำหรับกิจกรรมกลางแจ้งและการท่องเที่ยว"));
		
	}

	@EventMapping
	public Message handleTextMessage(MessageEvent<TextMessageContent> e) {
		System.out.println("event: " + e);
		TextMessageContent message = e.getMessage();
		return service.CheckAction(message.getText());

	}

	@EventMapping
    public Message handleLocationMessage(MessageEvent<LocationMessageContent> e) {
         LocationMessageContent message = e.getMessage();
				return service.Getlocation(message.getLatitude(),message.getLongitude());
    }


}
