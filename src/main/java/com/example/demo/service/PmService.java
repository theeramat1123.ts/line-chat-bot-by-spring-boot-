package com.example.demo.service;

import java.util.List;
import java.util.Set;
import com.example.demo.entity.StationEntity;
import com.example.demo.repository.LevelRepository;
import com.example.demo.repository.StationRepository;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.*;

/**
 * Setlevelbypm
 */
@Service
 public class PmService {

    @Autowired
    private StationRepository stationRepository;
    @Autowired
    private LevelRepository levelRepository;

    public int setLevel (int pmCur){
        int level = 0;
        if (pmCur <= 25) {
            level = 1;
        } else if (pmCur <= 50) {
            level = 2;
        } else if (pmCur <= 100) {
            level = 3;
        } else if (pmCur <= 200) {
            level = 4;
        } else if (pmCur > 201) {
            level = 5;
        }
        return level;
    }


    public String getlevelbyLevelString(int currentLevel) {

        LevelEntity level = levelRepository.findByLevel(currentLevel);
        return level.getRespon();
    }

    public StationEntity Nearrest(double lat, double longs, List<StationEntity> stations) {
        Double min =  0.00;
        int index = 0;
        for (int i = 0; i < stations.size(); i++) {
            Double power1 = stations.get(i).getLag() - lat;
            Double power2 = stations.get(i).getLong() - longs;
        Double total = Math.sqrt(Math.pow(power1, 2)+Math.pow(power2, 2));
        if(total<min){
          min = total;
          index = i;
        }
     }
     return stations.get(index);
}
public Message CheckAction(String action) {
    switch(action){
        case "เช็คฝุ่น":
        return new TextMessage("ขอ Location หน่อย");
        default:
        return new TextMessage("พิมพ์ เช็คฝุ่น เพื่อตรวจสอบค่า PM2.5 ");
    }
}
public Message Getlocation(double lat,double longs ) {
    List<StationEntity> stations = (List<StationEntity>) stationRepository.findAll();

    StationEntity station = Nearrest(lat,longs,stations);
    int level = setLevel(station.getpmCur());
    String Level=getlevelbyLevelString(level);
    return new TextMessage("สถานี:" + station.getName()+" "+Level);
}



}
