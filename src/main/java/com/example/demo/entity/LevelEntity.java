package com.example.demo.entity;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class LevelEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int Id ;
    private int level ;
    private String type ;
    private String respon ;
    
    public LevelEntity() {
    }
    
    public LevelEntity(int i, int j, String string, String string2) {
        Id = i;
        level = j;
        type = string;
        respon = string2;
	}
	public void setLevel(int level) {
        this.level = level;
    }
    public int getLevel() {
        return level;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getType() {
        return type;
    }
    public void setRespon(String respon) {
        this.respon = respon;
    }
    public String getRespon() {
        return respon;
    }
}