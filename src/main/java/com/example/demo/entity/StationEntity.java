package com.example.demo.entity;
import java.lang.reflect.Constructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class StationEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int Id ;
    private String Name ;
    private int pmCur ;
    private double lag ;
    private double longs ;
    
    public StationEntity(){
    }
    
    public StationEntity(int id,String name,int pmCur,double lag,double longs){
        this.Id = id;
        this.Name = name;
        this.pmCur=pmCur;
        this.lag=lag;
        this.longs=longs;
    }
    
    public void setName(String name) {
        this.Name = name;
    }
    public String getName() {
        return Name;
    }
    public void setpmCur(int pm) {
        this.pmCur = pm;
    }
    public int getpmCur() {
        return pmCur;
    }
    public void setLag(double lag) {
        this.lag = lag;
    }
    public double getLag() {
        return lag;
    }
    public void setLongs(double longs){
        this.longs = longs;
    }
    public double getLong(){
        return longs;
    }
}